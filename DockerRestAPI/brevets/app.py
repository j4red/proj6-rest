import os, logging, flask, json
from flask import Flask, redirect, url_for, request, render_template
from flask_restful import Resource, Api
from pymongo import MongoClient
import acp_times, config

# ========== Globals ==========
app = Flask(__name__)
api = Api(app)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY

# Step 1: Create a client object.
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
# Step 2: Connect to the DB.
db = client.acp

# ========== Pages ==========
@app.route("/")
@app.route("/index")
def index():
	return flask.render_template('calc.html')

@app.route('/display')
def display():
	_items = db.brevets.find() # Load collection named brevets
	items = [item for item in _items]
	sorted_items = sorted(items, key=lambda k: k['km'])
	return render_template('display.html', items=sorted_items)

@app.errorhandler(404)
def page_not_found(error):
	app.logger.debug("Page not found")
	flask.session['linkback'] = flask.url_for("index")
	return flask.render_template('404.html'), 404

# ===== AJAX request handlers =====
@app.route("/_calc_times")
def _calc_times():
	"""
	Calculates open/close times from miles, using rules
	described at https://rusa.org/octime_alg.html.
	Expects one URL-encoded argument, the number of miles.
	"""
	# app.logger.debug("Got a JSON request")
	km = request.args.get('km', 999, type=float)
	app.logger.debug("km={}".format(km))
	app.logger.debug("request.args: {}".format(request.args))

	selected_dist = request.args.get('selected_dist', 1000, type=int)
	app.logger.debug("selected_dist={}".format(selected_dist))

	initial = request.args.get('initial', type=str)
	app.logger.debug("initial={}".format(initial))

	open_time = acp_times.open_time(km, selected_dist, initial)
	close_time = acp_times.close_time(km, selected_dist, initial)

	result = {
		"open": open_time,
		"close": close_time
	}
	return flask.jsonify(result=result)

@app.route("/_drop", methods=['POST'])
def _drop():
	db.brevets.drop()
	return redirect(url_for('index'))

@app.route("/_add", methods=['POST'])
def _add():
	""" Append entry to MongoDB database. Helped greatly by the discussion at
		https://www.w3schools.com/JQuery/jquery_ajax_get_post.asp. """
	control_doc = {
		'miles': request.form['miles'],
		'km': float(request.form['km']),
		'location': request.form['location'],
		'open': request.form['open'],
		'close': request.form['close'],
		'note': request.form['note'].replace("<br>", "") # remove <br>
	}

	app.logger.debug(f"inserting {control_doc} into db")
	db.brevets.insert_one(control_doc)
	return flask.jsonify(status=True)

# ===== Resources =====
class AllResults(Resource):
	""" Resource Demo. """
	def get(self):
		_items = db.brevets.find() # Load collection
		items = [item for item in _items] # format collection as list
		app.logger.debug(f"\n\nitems: {type(items)}; {items}\n")
		sorted_items = sorted(items, key=lambda k: k['km'])

		objects = []
		for i in sorted_items:
			obj = {
				"location" : i["location"],
				"miles" : i["miles"],
				"km" : i["km"],
				"open" : i["open"],
				"close" : i["close"],
				"note" : i["note"]
			}
			objects.append(obj)

		return { 'Controls': objects }
api.add_resource(AllResults, '/Results')

# I was previously using an HTML template to render the CSVs, but upon reading
# a post on Piazza from today, realized that apparently that's not the way to do
# it! This CSV display doesn't work well on the HTML page itself, but looks
# alright in the console.

# === Open and Close Times ====
class OpenCloseJSON(Resource):
	def get(self):
		_items = db.brevets.find() # Load collection
		items = [item for item in _items] # format collection as list
		sorted_items = sorted(items, key=lambda k: k['km'])

		try:
			top_k = int(request.args.get('top'))
		except:
			top_k = len(sorted_items)

		objects = []
		for i in sorted_items[:top_k]:
			obj = {
				"open" : i["open"],
				"close" : i["close"],
			}
			objects.append(obj)

		return { 'Open_Close_Times': objects }
api.add_resource(OpenCloseJSON, '/listAll', '/listAll/json')

class OpenCloseCSV(Resource):
	def get(self):
		_items = db.brevets.find() # Load collection
		items = [item for item in _items] # format collection as list
		sorted_items = sorted(items, key=lambda k: k['km'])
		try:
			top_k = int(request.args.get('top'))
		except:
			top_k = len(sorted_items)

		return_str = "Open,Close\n"
		for i in sorted_items[:top_k]:
			return_str += f"{i['open']},{i['close']}\n"

		return flask.make_response(return_str)
api.add_resource(OpenCloseCSV, '/listAll/csv')

# ======== Only Open Times =======
class OpenJSON(Resource):
	def get(self):
		_items = db.brevets.find() # Load collection
		items = [item for item in _items] # format collection as list
		sorted_items = sorted(items, key=lambda k: k['km'])

		try:
			top_k = int(request.args.get('top'))
		except:
			top_k = len(sorted_items)

		objects = []
		for i in sorted_items[:top_k]:
			objects.append(i["open"])

		return { 'Open_Times': objects }
api.add_resource(OpenJSON, '/listOpenOnly', '/listOpenOnly/json', '/listOnlyOpen', '/listOnlyOpen/json')

class OpenCSV(Resource):
	def get(self):
		_items = db.brevets.find() # Load collection
		items = [item for item in _items] # format collection as list
		sorted_items = sorted(items, key=lambda k: k['km'])

		try:
			top_k = int(request.args.get('top'))
		except:
			top_k = len(sorted_items)

		return_str = "Open\n"
		for i in sorted_items[:top_k]:
			return_str += f"{i['open']}\n"
		return flask.make_response(return_str)
api.add_resource(OpenCSV, '/listOnlyOpen/csv', '/listOpenOnly/csv')

# ======== Only Close Times =======
class CloseJSON(Resource):
	def get(self):
		_items = db.brevets.find() # Load collection
		items = [item for item in _items] # format collection as list
		sorted_items = sorted(items, key=lambda k: k['km'])

		try:
			top_k = int(request.args.get('top'))
		except:
			top_k = len(sorted_items)

		objects = []
		for i in sorted_items[:top_k]:
			objects.append(i["close"])

		return { 'Close_Times': objects }
api.add_resource(CloseJSON, '/listCloseOnly', '/listCloseOnly/json', '/listOnlyClose', '/listOnlyClose/json')

class CloseCSV(Resource):
	def get(self):
		_items = db.brevets.find() # Load collection
		items = [item for item in _items] # format collection as list
		sorted_items = sorted(items, key=lambda k: k['km'])

		try:
			top_k = int(request.args.get('top'))
		except:
			top_k = len(sorted_items)

		return_str = "Close\n"

		for i in sorted_items[:top_k]:
			return_str += f"{i['close']}\n"
		return flask.make_response(return_str)
api.add_resource(CloseCSV, '/listCloseOnly/csv', '/listOnlyClose/csv')

# ========== Application ==========
app.debug = CONFIG.DEBUG
if app.debug:
	app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
	print("Opening for global access on port 80.")
	app.run(host='0.0.0.0', port=80, debug=True)
