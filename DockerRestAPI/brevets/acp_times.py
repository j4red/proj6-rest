"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    # get time object from input ISO time
    start = arrow.get(brevet_start_time)

    # clamp KM to a [0, brevet_dist_km] range, then round. Based on:
    # https://stackoverflow.com/questions/4092528/how-to-clamp-an-integer-to-some-range
    control_dist_km = sorted((0, control_dist_km, brevet_dist_km))[1]
    control_dist_km = round(control_dist_km)

    remaining_dist = control_dist_km
    delay = 0 # initial delay is none

    if remaining_dist <= 200: # total <= 200
        delay += remaining_dist / 34
    else: # total > 200
        delay += 200 / 34
        remaining_dist -= 200

        if remaining_dist <= 200: # total in (200, 400]
            delay += remaining_dist / 32
        else: # total > 400
            delay += 200 / 32
            remaining_dist -= 200

            if remaining_dist <= 200: # total in (400, 600]
                delay += remaining_dist / 30
            else: # total > 600
                delay += 200 / 30
                remaining_dist -= 200

                if remaining_dist <= 400: # total in (600, 1000]
                    delay += remaining_dist / 28
                else: # total > 1000
                    delay += 400 / 28
                    remaining_dist -= 400

                    delay += remaining_dist / 26

    hrs = delay // 1 # get integer number of hours from delay
    mins = round((delay % 1) * 60) # get minutes to shift

    time = start.shift(hours=hrs, minutes=mins)

    # round to nearest minute
    time.shift(minutes=0.5).floor('minute')

    return time.isoformat()


def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
          brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    durations = { # total durations for valid brevet lengths
        200:  13.5,
        300:  20.0,
        400:  27.0,
        600:  40.0,
        1000: 75.0
    }

    # get time object from input ISO time
    start = arrow.get(brevet_start_time)
    # constrain to positive and round to nearest km
    control_dist_km = round(max(0, control_dist_km))

    remaining_dist = control_dist_km
    delay = 0

    if control_dist_km <= 60: # French variation
        delay += (remaining_dist / 20) + 1 # 20 KM/hr + 1hr
        remaining_dist = 0
    else:
        if control_dist_km <= 600:
            delay += remaining_dist / 15
            remaining_dist = 0
        else:
            delay += 600 / 15
            remaining_dist -= 600
            if remaining_dist <= 400: # total dist in 600..1000
                delay += remaining_dist / 11.428
                remaining_dist = 0
            else: # total distance > 1000
                delay += remaining_dist / 13.333

    if control_dist_km >= brevet_dist_km: # if final control point:
        if brevet_dist_km not in durations:
            print("Warning: specified brevet distanced not supported by official ACP regulations.")
        else:
            delay = durations[brevet_dist_km] # -> table lookup

    hrs = delay // 1
    mins = round((delay % 1) * 60)

    time = start.shift(hours=hrs, minutes=mins)

    # round to nearest minute
    time.shift(minutes=0.5).floor('minute')

    return time.isoformat()
