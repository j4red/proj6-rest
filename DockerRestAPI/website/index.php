<html>
    <head>
        <title>Brevets Calculator Display for CIS 322</title>
    </head>

    <body>
        <h1>Open and Close Times:</h1>
        <ul>
        <?php
            $json = file_get_contents('http://app/listAll');
            $obj = json_decode($json);

	        $times = $obj->Open_Close_Times;
            foreach ($times as $t) {
                echo "<li> Open: $t->open</li>";
                echo "<li>Close: $t->close</li>";
                echo "<br />";
            }
        ?>
        </ul>

        <h1>Open Times:</h1>
        <ul>
        <?php
            $json = file_get_contents('http://app/listOpenOnly');
            $obj = json_decode($json);
	        $array = $obj->Open_Times;
            foreach ($array as $a) {
                echo "<li>$a</li>";
            }
        ?>
        </ul>

        <h1>Close Times:</h1>
        <ul>
        <?php
            $json = file_get_contents('http://app/listCloseOnly');
            $obj = json_decode($json);
	        $array = $obj->Close_Times;
            foreach ($array as $a) {
                echo "<li>$a</li>";
            }
        ?>
        </ul>
    </body>
</html>
